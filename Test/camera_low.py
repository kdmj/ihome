from picamera import PiCamera
import time
#from time import sleep
from fractions import Fraction
import datetime

Hours_int = 0
Minuts_int = 0

#camera = PiCamera(framerate=Fraction(1,6))

camera = PiCamera(
    resolution=(1280, 720),
    framerate=Fraction(1, 6),
    sensor_mode=3)

# You can change these as needed. Six seconds (6000000)
# is the max for shutter speed and 800 is the max for ISO.
#camera.resolution = (720,720)
#camera.resolution = (3280,2464)
camera.rotation = 180
camera.shutter_speed = 2500000
camera.iso = 800

#time.sleep(30)
camera.exposure_mode = 'off'

i = 0

while True:
	if (Hours_int == 6 and Minuts_int > 10):
		break
	else:
		cur_time = datetime.datetime.now()
		stub = cur_time.strftime("%Y%m%d%H%M%S_low")
		Hours_int = int(cur_time.strftime("%H"))
		Minuts_int = int(cur_time.strftime("%M"))
		outfile = "%s.jpg" % (stub)
		camera.capture(outfile)
		time.sleep(1)
		i+=1

camera.close()


