
from picamera import PiCamera
from time import sleep
import time
import datetime

camera = PiCamera()
camera.rotation = 180
camera.resolution = (640,480)
camera.start_preview()

i = 0
for i in range (5):
	current_time = datetime.datetime.now()
	stub = current_time.strftime("%Y%m%d%H%M%S")
	outfilename = "%s.jpg" % (stub)
	camera.capture(outfilename)
	sleep(5)
	i +=1


sleep(5)

camera.stop_preview()

